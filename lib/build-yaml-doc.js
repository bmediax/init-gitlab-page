const createYAMLCollection = require('./create-yaml-collection')

module.exports = publicExists => {
  const shellCommands = {
    ifPublicExists: [
      `echo "Using project \\"public\\" directory"`
    ],
    ifPublicDoesNotExist: [
      `echo "Creating \\(then using\\) new \\"public\\" directory from files at project root"`,
      `mkdir ./.public`,
      `cp -r * ./.public`,
      `mv ./.public ./public`
    ]
  }

  const deploymentShellCommands = publicExists
    ? createYAMLCollection(shellCommands.ifPublicExists)
    : createYAMLCollection(shellCommands.ifPublicDoesNotExist)

  return (
`pages:
  stage: deploy
  script: ${deploymentShellCommands}
  artifacts:
    paths:
      - public
  only:
    - master`
  )
}
